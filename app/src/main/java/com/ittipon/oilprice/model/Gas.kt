package com.ittipon.oilprice.model

import androidx.annotation.StringRes

data class Gas(@StringRes val stringResourcePrice: Int, @StringRes val StringResourceTitle: Int)
